import React, { useState } from "react";
import { Modal, Button, Badge } from "react-bootstrap";

function Notification(props) {
    const [showModal, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
            <div>
                <Button variant="primary" onClick={handleShow}>
                    Notifications<Badge bg="danger" style={{position:"relative",top:"-70%",left:"30%"}}>{props.Notification.length}</Badge>
                    <span className="visually-hidden"></span>
                </Button>
            </div>
            <Modal show={showModal} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Notification</Modal.Title>
                </Modal.Header>
                <Modal.Body>{props.Notification.map(n => <p key={n}>{n}</p>)}</Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={handleClose}>
                        Close
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}