import React from 'react'
import Modal from './Modal'
import { useState } from 'react'

export default function Notification(props) {
    const [a, seta] = useState(0)

    const modalNotification = () => {
        if (a !== 0) {
            return (<Modal Notifications={props.Notification} />)
        }
    }
    return (
        <>
            <button type="button" className="btn btn-primary" onClick={(e) => {

                    e.preventDefault()
                    seta(1)
                }}>
                Notifications <span className="badge bg-secondary" >
                    {props.Notification.length}</span>
            </button>
            {modalNotification()}
        </>
    )
}
